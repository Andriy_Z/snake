﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Snake
{
    public class FoodGenerator : MonoBehaviour
    {
        [SerializeField] private Transform FoodPrefab;

        private Transform _food;

        private int _gridSize;
        private float _cellsize;
        private int _randomAttemptsCount=30;
        public void Init(int gridSize, float cellsize)
        {
            _gridSize = gridSize;
            _cellsize = cellsize;
        }

        public void HideFood()
        {
           GetFoodTransform().gameObject.SetActive(false);
        }

        public Vector3 GenerateFood(List<SnakeClass> snake)
        {
            List<Vector3> positions = new List<Vector3> {snake[0].TargetPosition};
            positions.AddRange(snake.Select(t => t.ElementPosition));


            for (int i = 0; i < _randomAttemptsCount; i++)
            {
                int x = Random.Range(0, _gridSize);
                int z = Random.Range(0, _gridSize);
                Vector3 position = new Vector3(x,FoodPrefab.transform.position.y,z);
                position *= _cellsize;

                if (!positions.Contains(position))
                {
                    GetFoodTransform().localPosition = position;
                    return position;
                }
            }
            int xLast = Random.Range(0, _gridSize);
            int zLast = Random.Range(0, _gridSize);
            Vector3 positionLast = new Vector3(xLast, FoodPrefab.transform.position.y, zLast);
            positionLast *= _cellsize;
            GetFoodTransform().localPosition = positionLast;
            return positionLast;
        }

        private Transform GetFoodTransform()
        {
            if (_food == null)
            {
                _food = Instantiate(FoodPrefab,transform);
                _food.localScale *= _cellsize;
            }
            _food.gameObject.SetActive(true);
            return _food;
        }

    }
}
