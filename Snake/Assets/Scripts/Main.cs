﻿using System.Collections;
using UnityEngine;

namespace Snake
{
    public class Main : MonoBehaviour
    {
        [SerializeField] private UIMain _uiMain;
        private Cells Cells => _cells ?? (_cells = GetComponent<Cells>());
        private Cells _cells;
        private FoodGenerator FoodGenerator => _foodGenerator ?? (_foodGenerator = GetComponent<FoodGenerator>());
        private FoodGenerator _foodGenerator;
        private SnakeMoveControll SnakeMoveControll=>_snakeMoveControll ??(_snakeMoveControll = GetComponent<SnakeMoveControll>());
        private SnakeMoveControll _snakeMoveControll;

        [SerializeField] private int _gridDimention = 18;
        [SerializeField] private float _cellSize = .1f;

        [SerializeField] private int _initSnakelength=5;
        [SerializeField] private int _initLifeCount=3;

        private float _foodFrequence=2f;
        private int _currentLifesCount;
        private int _currentSnakeLength;
        private int _points;

        private void Pause()
        {
            _snakeMoveControll._move = false;
        }

        private void Unpause()
        {
            _snakeMoveControll._move = true;
        }

        private IEnumerator  Start()
        {
            Cells.Init(_gridDimention,_cellSize);
            SnakeMoveControll.Init(_gridDimention, _cellSize,_initSnakelength);
            FoodGenerator.Init(_gridDimention,_cellSize);
            _uiMain.Init();
            yield return new WaitForSeconds(1f);
            Restart();
        }

        #region InGame methods

        public void FoodEated()
        {
            _foodGenerator.HideFood();
            _points++;
            _currentSnakeLength++;
            ShowInGameData();
            Invoke("CreateFood",_foodFrequence);
        }

        private void CreateFood()
        {
           Vector3 pos =  _foodGenerator.GenerateFood(_snakeMoveControll.Snake);
            _snakeMoveControll.AddFood(pos);
        }

        private void Continue()
        {
            _currentSnakeLength = _initSnakelength;
            StartCoroutine(_snakeMoveControll.InitSnake());
            ShowInGameData(true);
            Invoke("CreateFood", _foodFrequence);
            _uiMain.SetActiveGameElements(true,Pause,Unpause);
        }

        private void Restart()
        {
            _points = 0;
            _currentLifesCount = _initLifeCount;
            _currentSnakeLength = _initSnakelength;
            StartCoroutine(_snakeMoveControll.InitSnake());
            ShowInGameData(true);
         
            Invoke("CreateFood", _foodFrequence);
            _uiMain.SetActiveGameElements(true,Pause, Unpause);
        }

        private void ShowInGameData(bool smooth = false)
        {
            _uiMain.SetGameInfo(_points,_currentLifesCount,_currentSnakeLength,smooth);
        }

        private void Exit()
        {
            Application.Quit();
        }

        public void LifeLosted()
        {
            _uiMain.SetActiveGameElements(false,null,null);

            _foodGenerator.HideFood();
            if (IsInvoking("CreateFood")) CancelInvoke("CreateFood");

            if (--_currentLifesCount>0)
            {
                _uiMain.ActivatePopup("You have lost one life", "Restart", Restart, true, "Continue", Continue);
            }
            else
            {
#if UNITY_ANDROID
                _uiMain.ActivatePopup("You Loose", "Restart", Restart, true, "Exit", Exit);
#else 
                 _uiMain.ActivatePopup("You Loose", "Restart", Restart);
#endif
            }

        }

#endregion
    }
}
