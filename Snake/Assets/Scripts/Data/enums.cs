﻿using System.Collections.Generic;

namespace Snake
{
    public enum Directions
    {
        Up,
        Right,
        Left,
        Down
    }

    public enum SceneNames
    {
        InitScene,
        Snake3D,
        SnakeAR
    }

    public static class SceneNamesDictionary
    {
        private static readonly Dictionary<SceneNames, string> dict = new Dictionary<SceneNames, string>
        {
            {SceneNames.InitScene, "InitScene"},
            {SceneNames.Snake3D, "Snake3D"},
            {SceneNames.SnakeAR, "SnakeAR"}
        };

        public static string GetStringValue(this SceneNames sceneNames)
        {
            string s = "";
            return dict.TryGetValue(sceneNames, out s) ? s : sceneNames.ToString();
        }
    }

}