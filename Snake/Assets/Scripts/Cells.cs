﻿using System.Collections;
using DG.Tweening;
using UnityEngine;

namespace Snake
{
    public class Cells : MonoBehaviour
    {
        private Cell[,] Cell;
        [SerializeField] private Transform CellPrefab;
        public void Init(int gridSize,float cellsize)
        {
            StartCoroutine(InitCells(gridSize, cellsize));
        }

        private IEnumerator InitCells(int gridSize,float cellSize)
        {
            transform.localPosition = new Vector3(-gridSize*cellSize / 2, transform.localPosition.y, -gridSize * cellSize / 2);
            Cell = new Cell[gridSize, gridSize];
            yield return new WaitForSeconds(0.3f);
            for (int i = 0; i < gridSize; i++)
            {
                for (int j = 0; j < gridSize; j++)
                {
                    Cell[i, j] = new Cell(CellPrefab, transform, new Vector3(i,CellPrefab.position.y, j), cellSize);
                    //Cell[i, j] = new Cell(CellPrefab, transform, new Vector3(i - gridSize / 2, CellPrefab.position.y, j - gridSize / 2), cellSize);//
                    Cell[i, j].CellTransform.localScale *= 0.1f;
                    Cell[i, j].CellTransform.DOScale(CellPrefab.localScale * cellSize, 0.2f);
                }
                yield return new WaitForSeconds(0.02f);
            }
            yield return new WaitForSeconds(2f);

        }
    }

    public class Cell : MonoBehaviour
    {
        public Cell(Transform prefab, Transform parent, Vector3 position, float cellsize)
        {
            position *= cellsize;
            CellTransform = Instantiate(prefab, position, Quaternion.identity, parent);
            CellTransform.localScale *= cellsize;
            CellTransform.localPosition = position;
            CellTransform.localRotation = Quaternion.identity;
            Position = position;
        }

        public Vector3 Position;
        public Transform CellTransform;

    }
}
