﻿using UnityEngine;

namespace Snake
{
    [RequireComponent(typeof(CanvasGroup))]
    public abstract class ViewBase : MonoBehaviour
    {
        private CanvasGroup _cGroup;
        protected CanvasGroup _canvasGroup => _cGroup ?? (_cGroup = GetComponent<CanvasGroup>());

        private RectTransform _rect;
        protected RectTransform _rectTransform => _rect ?? (_rect = GetComponent<RectTransform>());

        public virtual void Init()
        {
            _rectTransform.anchorMin = Vector2.zero;
            _rectTransform.anchorMax = Vector2.one;
        }

        public virtual void Enable()
        {
            _canvasGroup.alpha = 1;
            _canvasGroup.blocksRaycasts = true;
        }

        public virtual void Disable()
        {
            _canvasGroup.alpha = 0;
            _canvasGroup.blocksRaycasts = false;
        }
    }
}