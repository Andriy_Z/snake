﻿using System;
using System.Collections;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Snake
{
    public class ArPlaceView : ViewBase
    {
        [SerializeField] private Button _placeGroundPlaneButton;
        [SerializeField] private Button _try3DSnakeButton;
        [SerializeField] private DragButtonManager _dragButtonManager;

        [SerializeField] private TextMeshProUGUI _infoText;

        private float _uppearDuration = 0.3f;
        private bool _fitting;

        private readonly  string LOADING = "Loading";

        private UnityAction _pause;
        private UnityAction _unPause;
        public override void Init()
        {
            base.Init();
            Disable();
        }

        public void SetCallbacksData(UnityAction pause,UnityAction unPause)
        {
            _pause = pause;
            _unPause = unPause;
        }

        public override void Enable()
        {
            _fitting = false;
            base.Enable();
            DOTween.Kill(this);
            _placeGroundPlaneButton.GetComponent<RectTransform>().DOScale(1, _uppearDuration).SetEase(Ease.Linear).SetId(this);
            _placeGroundPlaneButton.onClick.AddListener(ActivateMoving);

            _try3DSnakeButton.onClick.AddListener(()=>StartCoroutine(OnTry3DButton()));
            _infoText.text = string.Empty;
        }

        public override void Disable()
        {
            base.Disable();
            _placeGroundPlaneButton.onClick.RemoveAllListeners();
            _placeGroundPlaneButton.GetComponent<RectTransform>().localScale = Vector3.zero;
            _try3DSnakeButton.onClick.RemoveAllListeners();
        }


        private void ActivateMoving()
        {
            if (!_fitting)
            {
                _pause?.Invoke();
                _dragButtonManager.gameObject.SetActive(true);
                ShowText("Drag field to change it's position");
            }
            else
            {
                _unPause?.Invoke();
                _dragButtonManager.gameObject.SetActive(false);
                _infoText.text=string.Empty;
            }
            _fitting = !_fitting;
        }

        private IEnumerator OnTry3DButton()
        {
                _try3DSnakeButton.onClick.RemoveAllListeners();
                StartCoroutine(SetText(LOADING));
                yield return new WaitForSeconds(1.5f);
                string scenemane = SceneNames.Snake3D.GetStringValue();

                AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(scenemane);

                while (!asyncLoad.isDone)
                {
                    yield return null;
                }
        }
        public void ShowText(string text)
        {
            _infoText.text = string.Empty;
            StartCoroutine(SetText(text));
        }

        private IEnumerator SetText(string text)
        {
            string s = string.Empty;
            foreach (var VARIABLE in text)
            {
                s += VARIABLE;
                _infoText.text = s + "_";
                yield return new WaitForSeconds(0.05f);
            }
            _infoText.text = s;
            yield return new WaitForSeconds(0.5f);
        }
    }
}
