﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace Snake
{


    public class DragButtonManager : MonoBehaviour, IDragHandler, IPointerDownHandler, IPointerUpHandler
    {
        [SerializeField] private Camera _mainCamera;

        private Transform _dragTransform;
        private Vector3 _dragCorrection;

        private LayerMask _layerMask;
        private bool _isDragging;

        private int _dragFingerIndex;

        private readonly string DRAGGING_LAYER = "DraggingLayer";
        private readonly string FLOOR_TAG = "Floor";

        private void Awake()
        {
            _layerMask = LayerMask.GetMask(DRAGGING_LAYER);
        }

        void SetDragCorrection(Vector3 rayPosition, Vector3 transformPosition)
        {
            _dragCorrection = rayPosition - transformPosition;
        }

        Vector3 GetCorrectedPos(Vector3 raypos, Vector3 transformPos)
        {
            Vector3 newpos = new Vector3(raypos.x - _dragCorrection.x, transformPos.y, raypos.z - _dragCorrection.z);
            newpos = raypos - _dragCorrection;
            return newpos;
        }


        public void OnDrag(PointerEventData eventData)
        {
            if (_dragFingerIndex != eventData.pointerId) return;

            if (_isDragging)
            {
                if (_dragTransform)
                {
                    RaycastHit hit;
                    if (Physics.Raycast(_mainCamera.ScreenPointToRay(eventData.position), out hit, 1000, _layerMask))
                    {
                        _dragTransform.position = GetCorrectedPos(hit.point, _dragTransform.position);
                    }
                }
            }
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            if (eventData.pointerId >= 0 && _dragFingerIndex == -1 || eventData.pointerId < 0)
            {
                _dragFingerIndex = eventData.pointerId;

                if (Physics.Raycast(_mainCamera.ScreenPointToRay(eventData.position), out var hit, 10000, ~_layerMask))
                {
                    if (hit.collider == null) return;

                    if (hit.transform.CompareTag(FLOOR_TAG) || hit.transform.name == "Emulator Ground Plane")
                    {
                        _dragTransform = hit.transform;
                        if (Physics.Raycast(_mainCamera.ScreenPointToRay(eventData.position), out hit, 1000,
                            _layerMask))
                        {
                            _isDragging = true;
                            SetDragCorrection(hit.point, _dragTransform.position);
                        }
                        else
                        {
                            _dragTransform = null;
                        }
                    }
                }
            }
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            _isDragging = false;
            _dragTransform = null;
            _dragFingerIndex = -1;
            _dragCorrection = Vector3.zero;
        }

    }
}