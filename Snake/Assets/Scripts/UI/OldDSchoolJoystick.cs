﻿using System.Collections;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace Snake
{
    public class OldDSchoolJoystick : MonoBehaviour
    {

        public static OldDSchoolJoystick Instance;


        [SerializeField] private Button LeftButton;
        [SerializeField] private Button RightButton;
        [SerializeField] private Button UpButton;
        [SerializeField] private Button DownButton;

        private Directions _direction;
        public Directions CurrentDirection { get; private set; }

        private void Awake()
        {
            Instance = this;
        }



        public void Enable()
        {
            LeftButton.onClick.AddListener(() =>
            {
                ChangeDirections(Directions.Left);
                DoFade(LeftButton.GetComponent<RectTransform>());
            });
            RightButton.onClick.AddListener(() =>
            {
                ChangeDirections(Directions.Right);
                DoFade(RightButton.GetComponent<RectTransform>());
            });

            UpButton.onClick.AddListener(() =>
            {
                ChangeDirections(Directions.Up);
                DoFade(UpButton.GetComponent<RectTransform>());
            });
            DownButton.onClick.AddListener(() =>
            {
                ChangeDirections(Directions.Down);
                DoFade(DownButton.GetComponent<RectTransform>());
            });
            StartCoroutine(EnableAnim());
        }

        public  void Disable()
        {
            LeftButton.onClick.RemoveAllListeners();
            RightButton.onClick.RemoveAllListeners();
            UpButton.onClick.RemoveAllListeners();
            DownButton.onClick.RemoveAllListeners();
            Vector3 scale = new Vector3(0.1f,0.1f,0.1f);
            LeftButton.GetComponent<RectTransform>().localScale = scale;
            RightButton.GetComponent<RectTransform>().localScale = scale;
            UpButton.GetComponent<RectTransform>().localScale = scale;
            DownButton.GetComponent<RectTransform>().localScale = scale;
        }

        private void DoFade(RectTransform rect)
        {
            rect.DOScale(0.9f, 0.1f).OnComplete(()=>rect.DOScale(1f, 0.1f));
        }

        private IEnumerator EnableAnim()
        {
           
            yield return null;
            float duration = 0.2f;

            // Animation
            LeftButton.GetComponent<RectTransform>().DOScale(1, duration).SetEase(Ease.Linear);
            yield return new WaitForSecondsRealtime(0.02f);
            RightButton.GetComponent<RectTransform>().DOScale(1, duration).SetEase(Ease.Linear);
            yield return new WaitForSecondsRealtime(0.02f);
            UpButton.GetComponent<RectTransform>().DOScale(1, duration).SetEase(Ease.Linear);
            yield return new WaitForSecondsRealtime(0.02f);
            DownButton.GetComponent<RectTransform>().DOScale(1, duration).SetEase(Ease.Linear);
            yield return new WaitForSecondsRealtime(0.02f);
            
            yield return null;
        }
        private void ChangeDirections(Directions newDir)
        {
            CurrentDirection = newDir;
        }
    }
}
