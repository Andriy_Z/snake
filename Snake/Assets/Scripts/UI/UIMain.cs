﻿using UnityEngine;
using UnityEngine.Events;

namespace Snake
{
    public class UIMain : MonoBehaviour
    {

        [SerializeField] private PopupView _popupView;
        [SerializeField] private GameInfoView _gameInfoView;
        [SerializeField] private JoysticView _joysticView;
        [SerializeField] private ArPlaceView _arPlaceView;
        public void Init()
        {
            _gameInfoView.Init();
            _popupView.Init();
            _joysticView.Init();
            if (_arPlaceView != null)
            {
                _arPlaceView.Init();
            }
        }

        public void SetGameInfo(int points, int lives, int snakeLength,bool smooth = false)
        {
            _gameInfoView.SetViewData(points, lives, snakeLength,smooth);
        }

   
        public void ActivatePopup(string infoText, string FirstButtonText = "OK", UnityAction FirstButtonAction = null, bool TwoButtonNotification = false,
            string SecondButtonText = null, UnityAction SecondButtonAction = null)
        {
            _popupView.ActivateView(infoText,FirstButtonText ,FirstButtonAction, TwoButtonNotification ,SecondButtonText , SecondButtonAction);
        }

        public void SetActiveGameElements(bool active,UnityAction pause, UnityAction unpause)
        {
            if (active) _joysticView.Enable();
            else _joysticView.Disable();

            if (_arPlaceView == null) return;

            if (active)
            {
                _arPlaceView.Enable();
                _arPlaceView.SetCallbacksData(pause,unpause);
                
            }
            else _arPlaceView.Disable();
        }
    }
}
