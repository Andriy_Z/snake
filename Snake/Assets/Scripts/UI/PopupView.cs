﻿using System.Collections;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Snake
{
    public class PopupView : ViewBase
    {
        [SerializeField] private Button _firstButton;
        [SerializeField] private TextMeshProUGUI _firstButtonText;
        [SerializeField] private Button _secondButton;
        [SerializeField] private TextMeshProUGUI _secondButtonText;
        [SerializeField] private TextMeshProUGUI _infoText;
        [SerializeField] private RectTransform[] AnimatedRects;

        [SerializeField] private Ease easeInType = Ease.Linear;
        [SerializeField] private float InDuration = 0.3f;
        [SerializeField] private float InDelay = 0.1f;

        [SerializeField] private Ease easeOutType = Ease.Linear;
        [SerializeField] private float OutDuration = 0.3f;
        [SerializeField] private float OutDelay = 0.1f;


        public override void Init()
        {
            base.Init();
            Reset();
        }

        public override void Enable()
        {
           StartCoroutine(AnimationIn());
           base.Enable();
        }

        public override void Disable()
        {
           StartCoroutine(AnimationOut(true));
        }

        private IEnumerator AnimationIn()
        {
            _infoText.text = infoTextArray;
            _firstButtonText.text = CloseButtonTextArray;
            _secondButtonText.text = DoButtonTextArray;
            _secondButton.gameObject.SetActive(TwoButtonCheckArray);

            yield return null;

            var duration = InDuration;

            _canvasGroup.DOFade(1, duration / 2);


            // Animation
            for (int i = 0; i < AnimatedRects.Length; i++)
            {
                AnimatedRects[i].DOScale(1, duration).SetEase(easeInType);

                yield return new WaitForSecondsRealtime(InDelay);
            }
            yield return null;
        }

        private IEnumerator AnimationOut(bool off = false, UnityAction action = null)
        {
            var duration = OutDuration;

            // Animation
            for (int i = 0; i < AnimatedRects.Length; i++)
            {
                AnimatedRects[i].DOScale(0, duration).SetEase(easeOutType);

                yield return new WaitForSecondsRealtime(OutDelay);
            }

            yield return new WaitForSecondsRealtime(duration);

            action?.Invoke();

            RemoveAllListeners();

            if (off) base.Disable();

            yield return null;
        }


        private string infoTextArray;
        private string CloseButtonTextArray;
        private bool TwoButtonCheckArray;
        private string DoButtonTextArray;

        public void ActivateView( string info,string FirstButtonText = "OK",UnityAction FirstButtonAction = null,bool 
            TwoButtonNotification = false,string SecondButtonText = null,UnityAction SecondButtonAction = null)
        {
            infoTextArray = info;
            CloseButtonTextArray = FirstButtonText;
            TwoButtonCheckArray = TwoButtonNotification;
            DoButtonTextArray = TwoButtonNotification ? SecondButtonText : null;

            RemoveAllListeners();

            _firstButton.onClick.AddListener(FirstButtonAction );
            _firstButton.onClick.AddListener(Disable);

            if (TwoButtonCheckArray)
            {
                _secondButton.onClick.AddListener(SecondButtonAction);
                _secondButton.onClick.AddListener(Disable);
            }
            Enable();
        }

        private void RemoveAllListeners()
        {
            _firstButton.onClick.RemoveAllListeners();
            _secondButton.onClick.RemoveAllListeners();
        }

        private void Reset()
        {
            for (int i = 0; i < AnimatedRects.Length; i++)
            {
                AnimatedRects[i].DOScale(0, 0);
            }

        }
    }
}