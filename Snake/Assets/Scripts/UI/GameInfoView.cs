﻿using System;
using System.Collections;
using TMPro;
using UnityEngine;
namespace Snake
{
    public class GameInfoView : ViewBase
    {
        [SerializeField] private TextMeshProUGUI PointsText;
        [SerializeField] private TextMeshProUGUI LivesText;
        [SerializeField] private TextMeshProUGUI SnakeLengthText;
        private string POINTS = "Points : ";
        private string LIVES = "Lives : ";
        private string SNAKE_LENGTH = "Snake length : ";


        public void SetViewData(int points, int lives, int snakeLength, bool smooth = false)
        {
            string pointsTxt = POINTS + points;
            string livesTxt = LIVES + lives;
            string snakeLengthTxt = SNAKE_LENGTH + snakeLength;
            if (smooth)
            {
                ResetTexts();
                
                StartCoroutine(SetText(pointsTxt, livesTxt, snakeLengthTxt));
            }
            else
            {
                PointsText.text = pointsTxt;
                LivesText.text  = livesTxt;
                SnakeLengthText.text =snakeLengthTxt;
            }
        }
        private IEnumerator SetText (string pointsTxt, string livesTxt, string snakeLengthTxt)
        {
            string s = string.Empty;
            foreach (var VARIABLE in pointsTxt)
            {
                s += VARIABLE;
                PointsText.text = s+"_";
                yield return new WaitForSeconds(0.05f);
            }
            PointsText.text = s;
            yield return new WaitForSeconds(0.1f);

            s = string.Empty;
            foreach (var VARIABLE in livesTxt)
            {
                s += VARIABLE;
                LivesText.text = s + "_";
                yield return new WaitForSeconds(0.05f);
            }
            LivesText.text = s;
            yield return new WaitForSeconds(0.1f);

            s = string.Empty;
            foreach (var VARIABLE in snakeLengthTxt)
            {
                s += VARIABLE;
                SnakeLengthText.text = s + "_";
                yield return new WaitForSeconds(0.05f);
            }
            SnakeLengthText.text = s;
            yield return new WaitForSeconds(0.1f);

        }
       
        private void ResetTexts()
        {
            PointsText.text =string.Empty;
            LivesText.text = string.Empty;
            SnakeLengthText.text = string.Empty;
        }
    }

}
