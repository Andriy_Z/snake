﻿using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.Events;

namespace Snake
{
    public class InitSceneView : ViewBase
    {
        public TextMeshProUGUI WelcomeText;
        public TextMeshProUGUI InfoText;

        private readonly string WELCOME = "Welcome to snake game!";
        private readonly string LOADING = "LOADING...";


        public void  ShowWelcomeText(UnityAction callbackAction=null)
        {
           StartCoroutine(SetText(WELCOME, WelcomeText,callbackAction));
        }

        public void ShowSupportsText (bool arSupported, UnityAction callbackAction = null)
        {
            StartCoroutine(SetText(GetSupportsText(arSupported),InfoText,callbackAction));
        }

        public void ShowLoadingText( UnityAction callbackAction = null)
        {
            InfoText.text = string.Empty;
            StartCoroutine(SetText(LOADING, WelcomeText, callbackAction));
        }

        private IEnumerator SetText(string text, TextMeshProUGUI tmproText,UnityAction callbackAction=null)
        {
            string s = string.Empty;
            foreach (var VARIABLE in text)
            {
                s += VARIABLE;
                tmproText.text = s + "_";
                yield return new WaitForSeconds(0.05f);
            }
            tmproText.text = s;
            yield return new WaitForSeconds(0.5f);
            callbackAction?.Invoke();
        }

        private string GetSupportsText(bool supported)
        {  
            return  supported ? 
                "Your device supports Vuforia ground plane technology" :
                "Your device did not support Vuforia ground plane technology";
        }

    }




}
