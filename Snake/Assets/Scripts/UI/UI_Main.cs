﻿using UnityEngine.UI;
using UnityEngine;

namespace Snake
{
    public class UI_Main : MonoBehaviour
    {
        public static UI_Main Instance;
        public Text Txt;
        public GameObject Joystick;
        public GameObject ResetButton;

        private void Awake()
        {
            Instance = this;
        }

        public void ShowMessage(string txt)
        {
            Txt.gameObject.SetActive(true);
            Txt.text = txt;
        }

        public void StageCompleted()
        {
            Joystick.SetActive(false);
            ResetButton.SetActive(true);
        }
    }

}