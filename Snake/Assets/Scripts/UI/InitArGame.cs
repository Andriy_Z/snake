﻿using UnityEngine;

namespace Snake
{
    public class InitArGame : MonoBehaviour
    {
        [SerializeField] private GameObject MainGameobject;
        [SerializeField] private GameObject UIMainGameobject;
        [SerializeField] private GameObject UIMainInit;
        [SerializeField] private GameObject PlaneFinder;


        private void Awake()
        {
            MainGameobject.SetActive(false);
            UIMainGameobject.SetActive(false);
        }
        

        public void InitGameObjects()  // called from ground plane finder
        {
            MainGameobject.SetActive(true);
            UIMainGameobject.SetActive(true);
            UIMainInit.SetActive(false);
            PlaneFinder.SetActive(false);
        }

    }
}