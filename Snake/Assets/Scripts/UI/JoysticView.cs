﻿using DG.Tweening;
using UnityEngine;

namespace Snake
{
    public class JoysticView : ViewBase
    {
        [SerializeField] private Transform Joystick;
        private OldDSchoolJoystick _oldDSchoolJoystick => _od ?? (_od = Joystick.GetComponent<OldDSchoolJoystick>());
        private OldDSchoolJoystick _od;
        private float _uppearDuration = 0.3f;

        public override void Init()
        {
            base.Init();
            ResetScale();
            Disable();
        }

        public override void Enable()
        {
            base.Enable();
            DOTween.Kill(this);
            Joystick.DOScale(1, _uppearDuration).SetEase(Ease.Linear).SetId(this);
            _oldDSchoolJoystick.Enable();
        }

        public override void Disable()
        {
            DOTween.Kill(this);
            base.Disable();
            ResetScale();
            _oldDSchoolJoystick.Disable();
        }

        private void ResetScale()
        {
            Joystick.localScale = Vector3.zero;
        }
    }
}