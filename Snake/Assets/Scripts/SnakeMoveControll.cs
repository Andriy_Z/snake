﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

namespace Snake
{
    public class SnakeMoveControll : MonoBehaviour
    {
        private struct food
        {
            public bool isInited;
            public Vector3 position;

            public food(bool isIninted,Vector3 pos)
            {
                position = pos;
                isInited = isIninted;
            }
        }
        private Main Main => _main ?? (_main = GetComponent<Main>());
        private Main _main;

        private Transform _transform => _tr ?? (_tr = GetComponent<Transform>());
        private Transform _tr;

        [SerializeField] private Transform SnakeElementPrefab;
        [SerializeField] private Transform CellPrefab;
        [SerializeField] private Directions _currentDirection;
        [SerializeField] private Directions _TargetDirection;
     
        private int _initSnakeLength;
        private int _gridDimention;
        private float _cellSize;

        [SerializeField] private  float _initspeed = 3f;
        [SerializeField] private float _currentSpeed;
        public List<SnakeClass> Snake { get; private set; }

        private Vector3 _initSnakePos; 
        public bool _move;
        private food _food; 
        public void Init(int gridDimention, float cellSize,int initSnakeLength)
        {
           
            _gridDimention = gridDimention;
            _initSnakeLength = initSnakeLength;
            _cellSize = cellSize;
            int i = Mathf.CeilToInt(gridDimention / 2);
            _initSnakePos = new Vector3(i, 0, i) * cellSize;
            _food = new food(false,Vector3.zero);
        }
        
        public IEnumerator InitSnake()
        {
            StopAllCoroutines();
            _currentDirection = Directions.Up;
            _TargetDirection = Directions.Up;
            _currentSpeed = _initspeed * _cellSize;
            yield return new WaitForSeconds(.5f);
            if (Snake != null && Snake.Count > 0)
            {
                for(int i = 0; i < Snake.Count; i++)
                {
                    Destroy(Snake[i].ElementTransform.gameObject);
                }
            }

            Snake = new List<SnakeClass>();
            Vector3 pos = _initSnakePos;

            for (int i = 0; i < _initSnakeLength; i++)
            {
                Transform tr =Instantiate(SnakeElementPrefab, pos, Quaternion.identity,_transform);
                tr.localPosition = pos;
                tr.name = $"snake_{i}";
                tr.localScale *= 0.1f;
                tr.DOScale(SnakeElementPrefab.localScale * _cellSize, 0.2f);
                Vector3 targetPos = GetTargetGrid(pos,_currentDirection);
                SnakeClass snake = new SnakeClass(tr,pos,targetPos,_cellSize);
                Snake.Add(snake);
                pos.z -= _cellSize;
            }
            yield return new WaitForSeconds(2f);
            _move = true;
        }

        public void AddFood(Vector3 pos)
        {
            _food = new food(true,pos);
        }

       public  void AddSnakeElement()
        {
            Vector3 initPosition = GetTailInitPosition(Snake[Snake.Count - 1].ElementTransform.position,Snake[Snake.Count - 1].CurrentDirection);
            Vector3 targetPos = Snake[Snake.Count - 1].ElementPosition;
            Transform tr = Instantiate(SnakeElementPrefab, initPosition, Quaternion.identity,_transform);
            tr.name = $"snake_{Snake.Count}";
            tr.localScale *= 0.1f;
            tr.DOScale(SnakeElementPrefab.localScale * _cellSize, .5f);
            SnakeClass snake = new SnakeClass(tr,initPosition,targetPos,_cellSize);
            Snake.Add(snake);
        }

        private void Move()
        {
            for (int i = 0; i < Snake.Count; i++)
            {
                Snake[i].ElementTransform.localPosition = Vector3.MoveTowards(Snake[i].ElementTransform.localPosition,Snake[i].TargetPosition, Time.deltaTime * _currentSpeed);

                if (Snake[i].TargetReached())
                {
                    Snake[i].SynkPosition();
                    if (i == 0)
                    {
                       _currentDirection = _TargetDirection;
                        Snake[0].TargetDirection = _currentDirection;
                    }
                    Directions dir =Snake[i].TargetDirection;
                    Snake[i].ElementPosition = Snake[i].TargetPosition;
                    Snake[i].TargetPosition = GetTargetGrid(Snake[i].ElementTransform.localPosition,dir);
                    Snake[i].CurrentDirection = dir;
                }
            }

            for (int i = 0; i < Snake.Count; i++)
            {
                Directions dir =Snake[i].CurrentDirection;
                if (i < Snake.Count - 1) Snake[i + 1].TargetDirection = dir;
            }

            CheckTargetPosition();
        }

        private void CheckTargetPosition()
        {
            for (int i = 1; i < Snake.Count; i++)
            {
                if (Snake[0].TargetPosition==Snake[i].ElementPosition)
                {
                    Debug.Log(Snake[i].ElementTransform.name+" collided tail".GetColoredString(Color.magenta));
                    StartCoroutine(LifeLosted());
                    return;
                }
            }

            if (Snake[0].TargetPosition.x >_gridDimention*_cellSize || Snake[0].ElementPosition.x <= 0 && Snake[0].TargetDirection == Directions.Left)
            {
                StartCoroutine(LifeLosted());
               
            }
            else if (Snake[0].TargetPosition.z > _gridDimention * _cellSize || Snake[0].ElementPosition.z <= 0 && Snake[0].TargetDirection == Directions.Down)
            {
                StartCoroutine(LifeLosted());
            }
            else if (_food.isInited)
            {
                if (Snake[0].TargetPosition == _food.position)
                {
                    Debug.Log("food".GetColoredString(Color.yellow));
                    Main.FoodEated();
                    AddSnakeElement();
                    _currentSpeed *= 1.1f;
                    _food = new food(false,Vector3.zero);
                }
            }

        }


        private IEnumerator LifeLosted()
        {
            _move = false;

            _food = new food(false, Vector3.zero);

            if (Snake[0] != null)
            {
                Main.LifeLosted();
                foreach (var VARIABLE in Snake)
                {
                    Rigidbody rbody = VARIABLE.ElementTransform.gameObject.AddComponent<Rigidbody>();
                    rbody.isKinematic = false;
                    rbody.useGravity = true;
                    VARIABLE.ElementTransform.DOScale(Vector3.zero, 2.5f);
                    yield return new WaitForSeconds(0.03f);
                }
            }
        }

        private void Update()
        {
            if (!_move) return;


            Directions newDirection = OldDSchoolJoystick.Instance.CurrentDirection;

            switch (newDirection)
            {
                case Directions.Up:
                    if (_currentDirection != Directions.Down) _TargetDirection = Directions.Up;
                    break;

                case Directions.Right:
                    if (_currentDirection != Directions.Left) _TargetDirection = Directions.Right;
                    break;

                case Directions.Left:
                    if (_currentDirection != Directions.Right) _TargetDirection = Directions.Left;
                    break;

                case Directions.Down:
                    if (_currentDirection != Directions.Up) _TargetDirection = Directions.Down;
                    break;
            }
          
            Move();
        }

        private Vector3 DirectionsToVector3(Directions dir)
        {
            switch (dir)
            {
                case Directions.Up:
                    return Vector3.forward*_cellSize;

                case Directions.Right:
                    return Vector3.right*_cellSize;

                case Directions.Left:
                    return -Vector3.right*_cellSize;

                case Directions.Down:
                    return -Vector3.forward*_cellSize;
            }
            return Vector3.forward*_cellSize;
        }
        private Vector3 GetTargetGrid(Vector3 pos, Directions direction)
        {
            pos += DirectionsToVector3(direction);
            return pos;
        }

        private Vector3 GetTailInitPosition(Vector3 pos,Directions direction)
        {
            pos -= DirectionsToVector3(direction);
            return pos;
        }

    }

    [Serializable]
    public class SnakeClass 
    {
        public SnakeClass(Transform trans,Vector3 pos,Vector3 targ,float cellsize)
        {
            
            ElementTransform = trans;
            parent = ElementTransform.parent;
            ElementPosition = pos;
            TargetPosition = targ;
            ElementTransform.localScale *= cellsize;
            ElementTransform.localRotation = Quaternion.identity;
           
        }


        public Transform ElementTransform;
        private Transform parent;
        public Directions TargetDirection;
        public Directions CurrentDirection;

        public Vector3 ElementPosition;
        private Vector3 _targPosition;

        public Vector3 TargetPosition;
        //{
        //    get {return _targPosition; }
        //    set {  _targPosition = parent.InverseTransformPoint(value); }
        //}

        public bool TargetReached()
        {
            return (ElementTransform.localPosition - TargetPosition).sqrMagnitude < 0.0001f;
        }

        public void SynkPosition()
        {
            ElementTransform.localPosition = TargetPosition;
        }

        public void RenewPosition()
        {
            ElementPosition = ElementTransform.position;
        }
       
    }
}