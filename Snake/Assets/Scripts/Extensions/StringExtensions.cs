﻿using System.Reflection;
using UnityEngine;

public static class StringExtensions
{
    public static string GetColoredString(this string s, Color color)
    {
        string colorname = "";
        PropertyInfo[] props = color.GetType().GetProperties(BindingFlags.Public | BindingFlags.Static);

        foreach (PropertyInfo prop in props)
        {
            if ((Color) prop.GetValue(null, null) == color)
            {
                colorname = prop.Name;
            }
        }

        if (colorname == "") colorname = color.ToString();

        string s1 = string.Format("<color={0}> {1} </color>", colorname, s);
        return s1;
    }
}
